<?php

use App\Models\Antrean;
use App\Models\Antrean_task;
use App\Models\Bed;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;

Route::post('antrean/add', function (Request $request) {
    if (Antrean::where('kodebooking', $request->kodebooking)->exists()) {
        return response()->json([
            'metadata' => [
                'message' => 'Data sudah ada',
                'code' => 208,
            ],
        ]);
    }

    Antrean::create([
        'kodebooking' => $request->kodebooking,
        'jenispasien' => $request->jenispasien,
        'nomorkartu' => $request->nomorkartu,
        'nik' => $request->nik,
        'nohp' => $request->nohp,
        'kodepoli' => $request->kodepoli,
        'namapoli' => $request->namapoli,
        'pasienbaru' => $request->pasienbaru,
        'norm' => $request->norm,
        'tanggalperiksa' => $request->tanggalperiksa,
        'kodedokter' => $request->kodedokter,
        'namadokter' => $request->namadokter,
        'jampraktek' => $request->jampraktek,
        'jeniskunjungan' => $request->jeniskunjungan,
        'nomorreferensi' => $request->nomorreferensi,
        'nomorantrean' => $request->nomorantrean,
        'angkaantrean' => $request->angkaantrean,
        'estimasidilayani' => $request->estimasidilayani,
        'sisakuotajkn' => $request->sisakuotajkn,
        'kuotajkn' => $request->kuotajkn,
        'sisakuotanonjkn' => $request->sisakuotanonjkn,
        'kuotanonjkn' => $request->kuotanonjkn,
        'keterangan_pasien' => $request->keterangan_pasien,
    ]);
    return response()->json([
        'metadata' => [
            'message' => 'Ok',
            'code' => 200,
        ],
    ]);
});
Route::post('antrean/batal', function (Request $request) {
    if (Antrean::where('kodebooking', $request->kodebooking)->doesntExist()) {
        return response()->json([
            'metadata' => [
                'message' => 'Data tidak ada',
                'code' => 201,
            ],
        ]);
    }
    Antrean::where('kodebooking', $request->kodebooking)->delete();
    return response()->json([
        'metadata' => [
            'message' => 'Ok',
            'code' => 200,
        ],
    ]);
});
Route::post('antrean/updatewaktu', function (Request $request) {
    if (Antrean_task::where('kodebooking', $request->kodebooking)->where('task', $request->taskid)->exists()) {
        return response()->json([
            'metadata' => [
                'message' => 'Data sudah ada',
                'code' => 208,
            ],
        ]);
    }
    Antrean_task::create([
        'kodebooking' => $request->kodebooking,
        'task' => $request->taskid,
        'time' => $request->waktu,
        'time_convert' => Carbon::createFromTimestampMs($request->waktu)->format('d-m-Y H:i:s'),
    ]);
    return response()->json([
        'metadata' => [
            'message' => 'Ok',
            'code' => 200,
        ],
    ]);
});

Route::post('/aplicaresws/rest/bed/update/{kodeppk}', function (Request $request, $kodeppk) {

    Bed::where('kode_ruang', $request->koderuang)->where('kode_kelas', $request->kodekelas)
        ->update([
            'kode_kelas' => $request->kodekelas,
            'nama_ruang' => $request->namaruang,
            'kapasitas' => $request->kapasitas,
            'tersedia' => $request->tersedia,
            'tersedia_pria' => $request->tersediapria,
            'tersedia_wanita' => $request->tersediawanita,
            'tersedia_pria_wanita' => $request->tersediapriawanita,
        ]);

    return response()->json([
        'metadata' => [
            'message' => 'Ok',
            'code' => 200,
        ],
    ]);
});
Route::post('/aplicaresws/rest/bed/create/{kodeppk}', function (Request $request, $kodeppk) {

    Bed::create([
        'kode_ruang' => $request->koderuang,
        'kode_ppk' => $kodeppk,
        'kode_kelas' => $request->kodekelas,
        'nama_ruang' => $request->namaruang,
        'kapasitas' => $request->kapasitas,
        'tersedia' => $request->tersedia,
        'tersedia_pria' => $request->tersediapria,
        'tersedia_wanita' => $request->tersediawanita,
        'tersedia_pria_wanita' => $request->tersediapriawanita,
    ]);

    return response()->json([
        'metadata' => [
            'message' => 'Ok',
            'code' => 200,
        ],
    ]);
});
Route::post('/aplicaresws/rest/bed/delete/{kodeppk}', function (Request $request, $kodeppk) {

    Bed::where('kode_ruang', $request->koderuang)->where('kode_kelas', $request->kodekelas)->delete();

    return response()->json([
        'metadata' => [
            'message' => 'Ok',
            'code' => 200,
        ],
    ]);
});
