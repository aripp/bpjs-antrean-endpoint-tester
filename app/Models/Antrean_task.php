<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Antrean_task extends Model
{
    use HasFactory;
    protected $table="antrean_task";
    protected $guarded = [];
}
