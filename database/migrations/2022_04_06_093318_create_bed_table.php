<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bed', function (Blueprint $table) {
            $table->id();
            $table->string("kode_ruang");
            $table->string("kode_ppk");
            $table->string("kode_kelas");
            $table->string("nama_ruang");
            $table->string("kapasitas");
            $table->string("tersedia");
            $table->string("tersedia_pria")->nullable();
            $table->string("tersedia_wanita")->nullable();
            $table->string("tersedia_pria_wanita")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bed');
    }
}
