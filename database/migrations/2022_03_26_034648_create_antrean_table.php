<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAntreanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antrean', function (Blueprint $table) {
            $table->id();
            $table->string('kodebooking')->unique();
            $table->string('jenispasien')->nullable();
            $table->string('nomorkartu')->nullable();
            $table->string('nik')->nullable();
            $table->string('nohp')->nullable();
            $table->string('kodepoli')->nullable();
            $table->string('namapoli')->nullable();
            $table->boolean('pasienbaru')->nullable();
            $table->string('norm')->nullable();
            $table->date('tanggalperiksa')->nullable();
            $table->string('kodedokter')->nullable();
            $table->string('namadokter')->nullable();
            $table->string('jampraktek')->nullable();
            $table->tinyInteger('jeniskunjungan')->nullable();
            $table->string('nomorreferensi')->nullable();
            $table->string('nomorantrean')->nullable();
            $table->string('angkaantrean')->nullable();
            $table->string('estimasidilayani')->nullable();
            $table->integer('sisakuotajkn')->nullable();
            $table->integer('kuotajkn')->nullable();
            $table->integer('sisakuotanonjkn')->nullable();
            $table->integer('kuotanonjkn')->nullable();
            $table->string('keterangan_pasien')->nullable();
            $table->boolean('deleted')->default(0);
            $table->string('deleted_keterangan')->nullable();
            $table->boolean('is_send_add')->default(false);
            $table->boolean('is_send_delete')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antrean');
    }
}
